<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Login - Membership System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="author">
	<link rel="shortcut icon" href="assets/images/favicon.ico">

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body>
	<div class="home-btn d-none d-sm-block"><a href="index.html" class="text-dark"><i class="fas fa-home h2"></i></a>
	</div>
	<div class="wrapper-page">
		<div class="card overflow-hidden account-card mx-3">
			<div class="bg-primary p-4 text-white text-center position-relative">
				<h4 class="font-20 m-b-5">Welcome Back !</h4>
				<p class="text-white-50 mb-4">Sign in to Membership System</p><a href="#" class="logo logo-admin"><img
						src="assets/images/logo-sm.png" height="24" alt="logo"></a>
			</div>
			<div class="account-card-content">
				<form class="form-horizontal m-t-30" action="<?php echo base_url('login/aksi_login'); ?>" method="post">
					<?php if($this->session->flashdata('msg') != NULL){?>
					<div class="alert alert-danger">
						<?php echo $this->session->flashdata('msg');?>
					</div>
					<?php } ?>

					<div class="form-group"><label for="user_name">Username</label> <input type="text" class="form-control"
							id="user_name" placeholder="Enter username" required></div>
					<div class="form-group"><label for="user_password">Password</label> <input type="password" class="form-control"
							id="user_password" placeholder="Enter password" required></div>
					<div class="form-group row m-t-20">
						<div class="col-sm-6">
							<!-- <div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input"
									id="customControlInline"> <label class="custom-control-label" for="customControlInline">Remember
									me</label></div> -->
						</div>
						<div class="col-sm-6 text-right"><button class="btn btn-primary w-md waves-effect waves-light"
								type="submit">Log In</button></div>
					</div>
					<div class="form-group m-t-10 mb-0 row">
						<div class="col-12 m-t-20"><a href="#"><i class="mdi mdi-lock"></i> Forgot your
								password?</a></div>
					</div>
				</form>
			</div>
		</div>
		<div class="m-t-40 text-center">
			<p>© 2020 Kingstech Service Pte. Ltd</p>
		</div>
	</div><!-- end wrapper-page -->
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>

</body>

</html>
