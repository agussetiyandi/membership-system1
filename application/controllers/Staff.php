<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Staff_model', 'staff');
        $this->load->model('Wilayah_model', 'provinsi');
    }

    public function index(){
        $resultStaff = $this->staff->getAllStaff()->result_array();
        $data = [
            'dataStaff' => $resultStaff
        ];
        $this->load->view('staff', $data);
    }

    public function add(){
        $this->load->view('addStaff');
    }

    public function simpan_staff(){
        // proses upload
        $file_name = '';

        $config['upload_path']          = './assets/images/staff';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2048;

        $this->load->library('upload', $config);
        if ( !$this->upload->do_upload('image_staff') ){
            $error = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
            $file_name = $data['file_name'];
        }

        // 2. Tangkap data dari form
        $insert_t_staff = [
            'staff_name' => $this->input->post('staffName'),
            'staff_email' => $this->input->post('staffEmail'),
            'staff_phone' => $this->input->post('staffPhone'),
            'staff_address' => $this->input->post('staffAddress'),
            'staff_photo' => $file_name
        ];

        // 3. Simpan ke database
        $this->staff->insertStaff($insert_t_staff);

        // 4. Update Staff
        function staffUpdate($staff_id){
          $data['staff'] = $this->Staff_model->get_data_staff($staff_id);
          $this->load->view('staffUpdate', $data);
        }
      
        function staffUpdateProcess(){
          $data = array(
            'perusahaan' => $this->input->post('perusahaan'),
            'nama' => $this->input->post('nama'),
            'jabatan' => $this->input->post('jabatan'),
            'mail' => $this->input->post('email'),
            'category' => $this->input->post('category')
          );
          $condition['id'] = $this->input->post('id');
          $this->Quotation_model->quotationUpdateProcessDB($data, $condition);
          redirect('Quotation/quotationList');
        }
      

        // 4. Alihkan ke halaman produk
        redirect('staff');

    }


}

?>