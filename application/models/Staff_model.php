<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_model extends CI_Model {

    public function getAllStaff() {
        return $this->db->get('t_staff');
    }

    public function getStaffById($id){
        return $this->db->get_where(['staff_id' => $id]);
    }

    public function insertStaff($data) {
        $this->db->insert('t_staff', $data);
    }

    function staffDeleteProsesDB($data){
      $this->db->where("staff_id", $data);
      $this->db->delete("t_staff");
    }
}