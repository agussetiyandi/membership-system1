<?php

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('Login_model');

	}

	function index(){
		$this->load->view('login');
	}

	function aksi_login(){
		$user_email = $this->input->post('user_email');
		$user_password = $this->input->post('user_password');
		$cek = $this->Login_model->cek_login($user_email,$user_password)->num_rows();
		if($cek > 0){
			$data = $this->Login_model->cek_login($user_email, $user_password)->row_array();
			$user_name = $data['user_name'];
			$user_level = $data['user_level'];
			$user_id = $data['user_id'];
			$user_position = $data['user_position'];

			$data_session = array(
				'user_id' => $user_id,
				'user_name' => $user_name,
        'user_position' => $user_position,
        'user_level' => $user_level,
				'status' => 'login'
			);

			$this->session->set_userdata($data_session);

			redirect('Dashboard');

		}else{
			echo $this->session->set_flashdata('msg','Email or Password is Wrong');
			redirect(base_url('login'));
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}
