<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Add Staff - Membership System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="author">
	<link rel="shortcut icon" href="assets/images/favicon.ico">

	<?php $this->load->view("_partials/css.php") ?>
</head>

<body>
	<?php $this->load->view("_partials/header.php") ?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="page-title-box">
					<div class="row align-items-center">
						<div class="col-sm-6">
							<h4 class="page-title">Add Staff</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
								<li class="breadcrumb-item"><a href="javascript:void(0);">Staff</a></li>
								<li class="breadcrumb-item active">Add Staff</li>
							</ol>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<form action="<?= base_url('staff/simpan_staff') ?>" method="post" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group row"><label for="staffName" class="col-lg-3 col-form-label">Name</label>
												<div class="col-lg-9"><input id="staffName" name="staffName" type="text" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group row"><label for="staffEmail" class="col-lg-3 col-form-label">Email</label>
												<div class="col-lg-9"><input id="staffEmail" name="staffEmail" type="text" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group row"><label for="staffPhone" class="col-lg-3 col-form-label">Phone</label>
												<div class="col-lg-9"><input id="staffPhone" name="staffPhone" type="text" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group row"><label for="staffAddress"
													class="col-lg-3 col-form-label">Address</label>
												<div class="col-lg-9"><textarea id="staffAddress" name="staffAddress" rows="4"
														class="form-control"></textarea></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group row"><label id="upload-image"
													class="col-lg-3 col-form-label">ID Photo</label>
													<div class="col-lg-9"><input type="file" name="image_staff" class="filestyle" data-buttonname="btn-secondary"></div>
											</div>
										</div>
									</div>

									<button id="btnSubmit" type="submit" class="btn btn-success" name="simpanData">Submit</button>
							</div>
							</form>
						</div>
					</div>
				</div><!-- end col -->
			</div><!-- end row -->

		</div><!-- container-fluid -->
	</div><!-- content -->
	<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<?php $this->load->view("_partials/js.php") ?>

</body>

</html>
